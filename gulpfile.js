var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var stripCssComments = require('gulp-strip-css-comments');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var headerfooter = require('gulp-headerfooter');
var foreach = require('gulp-foreach');
var uncss = require('gulp-uncss');
var rename = require("gulp-rename");
var replace = require('gulp-replace');
var fs = require('fs');
var clean = require('gulp-clean');
var path = require('path');
var htmlmin = require('gulp-htmlmin');
var gs = require('gulp-selectors');


var config = {
    bootstrapDir: './node_modules/bootstrap',
    faDir: './node_modules/font-awesome/',
    publicDir: './public',
    appDir: './app',
};
// Static Server + watching scss/html files
gulp.task('serve', function() {

    browserSync.init({
        open: false,
        server: config.publicDir
    });

    gulp.watch(config.appDir + "/css/**/*.scss", ['css']);
    gulp.watch(config.appDir + "/js/main.js", ['browserify']);
    gulp.watch(config.appDir + "/pages/*.html", ['buildPages']);
    gulp.watch(config.appDir + "/partials/*.html", ['buildPages']);
    gulp.watch(config.publicDir + "/*.html").on('change', browserSync.reload);
});


gulp.task('css', function() {
    return gulp.src(config.appDir + "/css/main.scss")
        .pipe(sass({
            includePaths: [
                config.bootstrapDir + '/scss',
                config.faDir + '/scss',
            ],
            outputStyle: 'compressed',
        }))
        .pipe(gulp.dest(config.publicDir + '/css'))
        .pipe(browserSync.stream());
});

gulp.task('buildPages', function() {
    return gulp.src(config.appDir + "/pages/*.html")
        .pipe(foreach(function(stream, file) {
            return stream
                .pipe(headerfooter.header(config.appDir + "/partials/header.html"))
                .pipe(headerfooter.footer(config.appDir + "/partials/footer.html"))
        }))
        .pipe(gulp.dest(config.publicDir + "/"));
});

gulp.task('buildCss', ['css', 'buildPages'], function() {
    return gulp.src(config.publicDir + "/*.html")
        .pipe(foreach(function(stream, file) {
            return gulp.src(config.publicDir + '/css/main.css')
                .pipe(stripCssComments({
                    preserve: false
                }))
                .pipe(uncss({
                    html: [file.path]
                }))
                .pipe(rename(path.basename(file.path, '.html') + '.css'))
                .pipe(gulp.dest('./target/'))
                .on('end', function() {
                    console.log('buildCss for ' + path.basename(file.path));
                });
        }));
});

gulp.task('minCSSnames', ['buildCss'], function() {

    return gulp.src([
            config.publicDir + "/*.html",
            './target/*.css',
        ])
        .pipe(gs.run({
            'css': ['css'],
            'html': ['html']
        }, {
            ids: ['desc', 'photo', 'specs']
        }))
        .pipe(gulp.dest('./target/'));
});

gulp.task('build', ['minCSSnames'], function() {
    gulp.src("./target/*.html")
        .pipe(foreach(function(stream, file) {
            return stream
                .pipe(replace(/<link rel="stylesheet" href="css\/main.css"[^>]*>/, function(s) {
                    var style = fs.readFileSync('./target/' + path.basename(file.path, '.html') + '.css', 'utf-8');
                    return '<style>' + style + '</style>';
                }));
        }))
        .pipe(htmlmin({
            collapseWhitespace: true
        }))
        .pipe(replace(/<!DOC.*<\/title>/, ''))
        .pipe(replace(/<\/head><body>/, ''))
        .pipe(replace(/<\/body><\/html>/, ''))
        .pipe(replace('src="', 'src="http://allegro.buksa.pl/'))
        .pipe(replace('url(../', 'url(http://allegro.buksa.pl/'))
        .pipe(replace('url("../', 'url("http://allegro.buksa.pl/'))
        .pipe(replace('!important', ''))
        .pipe(replace('cursor:not', 'cursor: not'))
        .pipe(gulp.dest('./target/'));
});

gulp.task('target', ['build', 'cleanBuild'], function() {});


gulp.task('clean', function() {
    gulp.src('./target/*.*')
        .pipe(clean());

    gulp.src(config.publicDir + '/**/*.{html,css}')
        .pipe(clean());
});

gulp.task('cleanBuild', ['build'], function() {
    gulp.src('./target/*.css')
        .pipe(clean());
});


gulp.task('default', ['css', 'buildPages', 'serve']);
